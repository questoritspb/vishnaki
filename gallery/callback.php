<!-- ********* -->
<!-- 1ST BLOCK -->
<section class="sect sect-fancy">
	<div class="content dis-fl fl-colnw jsc-flst alit-ct">
		<div class="slider" id="slider-plan-fancy">
			<div class="arrows dis-fl fl-rwnw jsc-spbet alit-ct" id="arrows_1"></div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-1.png" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-2.png" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-3.png" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-4.png" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-5.png" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/plan-6.png" alt="">
				</div>
			</div>
		</div>
		<div class="slider" id="slider-gallery">
			<div class="arrows dis-fl fl-rwnw jsc-spbet alit-ct" id="arrows_2"></div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/01.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/02.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/03.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/04.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/05.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/06.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/07.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/08.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/09.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/10.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/11.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/12.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/13.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/14.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/15.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/16.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/17.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/18.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/19.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/20.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/21.jpg" alt="">
				</div>
			</div>
			<div class="carousel-cell">
				<div class="img">
					<img src="../img/gallery/22.jpg" alt="">
				</div>
			</div>
		</div>
		<div class="container dis-fl fl-rwwr jsc-spbet alit-ct">
			<img src="../img/gallery/logo/letual.png" alt="">
			<button class="btn chng" type="button">смотреть схему этажей</button>
		</div>
	</div>
</section>
<!-- ********* -->
<!-- 2ND BLOCK -->
<section class="sect sect-callback gal clb" id="callback">
	<div class="content dis-fl fl-colnw jsc-flst alit-flst">
		<div class="title">
			<h3>хотите к нам?</h3>
		</div>
		<form action="../mailer.php" class="post-form" id="form-callback" name="form-callback" method="post" enctype="multipart/form-data" accept-charset="UTF-8" onsubmit="return valForm(this)">
			<div class="dis-fl fl-rwwr jsc-spbet alit-flst">
				<div class="input">
					<span>Имя</span>
					<input type="text" id="name" placeholder="" name="imya" required="">
				</div>
				<div class="input">
					<span>Телефон</span>
					<input type="tel" id="phone" placeholder="" name="phone" required="">
				</div>
				<div class="input">
					<span>Компания</span>
					<input type="email" id="email" placeholder="" name="email" required="">
				</div>
			</div>
			<button class="btn btn-form" type="submit">отправить</button>
		</form>
	</div>
</section>