<?
include_once 'config.php';
?>
<!doctype html>
<html lang="ru" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Торговый центр Вешняки - официальный сайт</title>
		<meta name="description" content="Официальный сайт торгового центра Вешняки. г. Москва, Вешняковская улица, 18">
		<meta name="keywords" content="ТЦ Вешняки, торговый центр">
		<meta name="og:description" content="ТЦ «Вешняки»">
		<link rel="canonical" href="https://tc-veshnyaki.ru/">
		<meta property="og:locale" content="ru_RU">
		<meta property="og:url" content="https://tc-veshnyaki.ru/">
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="tc-veshnyaki.ru">
		<meta property="og:title" content="Торговый центр Вешняки">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:image" content="img/tw-card.jpg">
		<meta name="twitter:title" content="Торговый центр Вешняки">
		<meta name="twitter:description" content="Торговый центр Вешняки">
		<link rel="icon" href="img/favicon.svg">
		<link rel="mask-icon" href="img/mask-icon.svg" color="#9fc321">
		<link rel="icon" href="img/favicon-32x32.png" sizes="32x32">
		<link rel="icon" href="img/favicon-64x64.png" sizes="64x64">
		<link rel="icon" href="img/favicon-192x192.png" sizes="192x192">
		<link rel="icon" href="img/favicon-512x512.png" sizes="512x512">
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
		<meta name="msapplication-TileImage" content="img/favicon-270x270.png">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="css/main.css?v=<?php echo filectime('css/main.css'); ?>">
		<link rel="stylesheet" href="css/custom.css?v=<?php echo filectime('css/custom.css'); ?>">
		<!-- for jsdelivr cdn -->
		<link rel="preconnect" href="https://cdn.jsdelivr.net">
		<!-- dns-prefetch only for IE11 --> 
		<!-- <link rel="dns-prefetch" href="https://cdn.jsdelivr.net"> -->
		<!-- livereload script / delete before deploy! -->
		<!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TN4J7NH');</script>
        <!-- End Google Tag Manager -->
        <?/*<!-- calltouch -->
        <script type="text/javascript">
            (function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","3oggdpha");
        </script>
        <!-- calltouch -->*/?>
	</head>
	<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN4J7NH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
	<?/*<div class="new-year-popup">
		<div class="new-year-block">
			<div class="close"></div>
            <img src="/img/banner-8-march.jpg" alt="">
		</div>
	</div>*/?>
		<!-- WRAP -->
		<main class="wrap-site dis-fl fl-colnw">
			<!-- ****** -->
			<!-- HEADER -->
			<header>
				<div class="container dis-fl fl-rwnw jsc-spbet alit-ct">
					<a href="/" class="logo">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168 46"><path d="M4.63,41V3.83H0V0H13V3.83H8.4V41H4.63Z" transform="translate(0 0)"/><path d="M28,41H15.46V0h3.82V37.24h6V0H29.1V37.29h2.57V46H28V41Z" transform="translate(0 0)"/><path d="M50.87,7a3.77,3.77,0,0,0-.6-2.44,2.38,2.38,0,0,0-2-.7H45.53V18.32h2.82a2.41,2.41,0,0,0,1.91-.65,3,3,0,0,0,.6-2.09V7Zm0.15,18a4.28,4.28,0,0,0-.55-2.49,2.33,2.33,0,0,0-2.11-.8H45.53V37.24H48.3q2.72,0,2.72-3.19V24.94ZM41.71,41V0H48.3Q51.62,0,53,1.64T54.44,7v8.21q0,3.83-2.52,4.53A3.77,3.77,0,0,1,54,21.06a4.74,4.74,0,0,1,.91,3.24v9.76q0,3.63-1.66,5.28a6.81,6.81,0,0,1-5,1.64H41.71Z" transform="translate(0 0)"/><path d="M58.07,41V0H69V3.83H61.9V18.22h5V22h-5V37.24h7.4V41H58.07Z" transform="translate(0 0)"/><path d="M72.18,0H76V37.24h7.55V0h3.82V37.24h8.25V0h3.82V41H72.18V0Z" transform="translate(0 0)"/><path d="M112.1,41V22h-5.33V41h-3.82V0h3.82V18.27h5.33V0h3.83V41H112.1Z" transform="translate(0 0)"/><path d="M119.65,41q0.75-4.83,1.61-9.61t1.86-9.81q-3.07-1.54-3.07-6V7.37q0-4,1.76-5.68A7.71,7.71,0,0,1,127.25,0h6.29V41h-3.82V22.4H127.2a2.34,2.34,0,0,1-.45,0q-0.86,4.68-1.56,9.26T123.83,41h-4.18Zm4.28-25.39a3.8,3.8,0,0,0,.75,2.59,3.35,3.35,0,0,0,2.52.8h2.52V3.83h-2.47a3.16,3.16,0,0,0-2.67.9,5.53,5.53,0,0,0-.65,3.14v7.72Z" transform="translate(0 0)"/><path d="M147.38,41l-7.25-20.16V41h-3.82V0h3.82V17.67l7-17.67h4.13l-7.85,18.67L151.76,41h-4.38Z" transform="translate(0 0)"/><path d="M153.76,41V0h3.37V21.76q0,4-.05,8.26l6.34-30H168V41h-3.32V20.16q0-4.43.05-9.11L157.58,41h-3.82Z" transform="translate(0 0)"/></svg>
					</a>
					<nav class="site-nav">
						<ul class="dis-fl fl-rwnw jsc-flend alit-ct">
							<li class="mob">
								<a href="#about">о нас</a>
							</li>
							<li class="mob">
								<a href="#arenda">арендаторы</a>
							</li>
							<li class="mob">
								<a href="/gallery/">галерея</a>
							</li>
							<li class="mob">
								<a href="#testimonials">нас любят</a>
							</li>
							<li class="mob">
								<a href="#wwd">мы одеваем и не только</a>
							</li>
							<li class="mob">
								<a href="#fcort">голодны?</a>
							</li>
							<li class="mob">
								<a href="#plan">карта этажей</a>
							</li>
                            <li class="mob">
                                <a href="/vacant-areas/">Свободные площади</a>
                            </li>
							<li class="mob">
								<a href="#contacts">контакты</a>
							</li>
							<li class="mob time">с 10.00 до 23.00</li>
							<li class="desk">
								<!-- !!! delete 'inactive' class before deploy !!! -->
								<a href="#" class="inactive">для арендаторов</a>
							</li>
							<li class="desk">
								<a href="#plan">карта этажей</a>
							</li>
							<li class="desk">
								<a href="/gallery/">галерея</a>
							</li>
							<li class="desk">
								<a href="#contacts">контакты</a>
							</li>
                            <li class="desk">
                                <a href="tel:<?=$config['phone_link']?>" class="phone"><?=$config['phone_text']?></a>
                            </li>
                            <li class="desk">
								<a href="/vacant-areas/" class="hdr btn">стать арендатором</a>
							</li>
						</ul>
					</nav>
                    <a href="tel:<?=$config['phone_link']?>" class="header__mobile-phone">
                        <img src="/img/call.svg" alt="Позвонить">
                    </a>
					<div class="menu-toggle">
						<div class="hamburger"></div>
					</div>
				</div>
			</header>
			<!-- ******* -->
			<!-- CONTENT -->
			<!-- ********* -->
			<!-- 1ST BLOCK -->
			<section class="sect sect-main" id="main">
				<div class="content dis-fl fl-colnw jsc-ct alit-ct">
					<h1>Центр притяжения!</h1>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168 46"><path d="M4.63,41V3.83H0V0H13V3.83H8.4V41H4.63Z" transform="translate(0 0)"/><path d="M28,41H15.46V0h3.82V37.24h6V0H29.1V37.29h2.57V46H28V41Z" transform="translate(0 0)"/><path d="M50.87,7a3.77,3.77,0,0,0-.6-2.44,2.38,2.38,0,0,0-2-.7H45.53V18.32h2.82a2.41,2.41,0,0,0,1.91-.65,3,3,0,0,0,.6-2.09V7Zm0.15,18a4.28,4.28,0,0,0-.55-2.49,2.33,2.33,0,0,0-2.11-.8H45.53V37.24H48.3q2.72,0,2.72-3.19V24.94ZM41.71,41V0H48.3Q51.62,0,53,1.64T54.44,7v8.21q0,3.83-2.52,4.53A3.77,3.77,0,0,1,54,21.06a4.74,4.74,0,0,1,.91,3.24v9.76q0,3.63-1.66,5.28a6.81,6.81,0,0,1-5,1.64H41.71Z" transform="translate(0 0)"/><path d="M58.07,41V0H69V3.83H61.9V18.22h5V22h-5V37.24h7.4V41H58.07Z" transform="translate(0 0)"/><path d="M72.18,0H76V37.24h7.55V0h3.82V37.24h8.25V0h3.82V41H72.18V0Z" transform="translate(0 0)"/><path d="M112.1,41V22h-5.33V41h-3.82V0h3.82V18.27h5.33V0h3.83V41H112.1Z" transform="translate(0 0)"/><path d="M119.65,41q0.75-4.83,1.61-9.61t1.86-9.81q-3.07-1.54-3.07-6V7.37q0-4,1.76-5.68A7.71,7.71,0,0,1,127.25,0h6.29V41h-3.82V22.4H127.2a2.34,2.34,0,0,1-.45,0q-0.86,4.68-1.56,9.26T123.83,41h-4.18Zm4.28-25.39a3.8,3.8,0,0,0,.75,2.59,3.35,3.35,0,0,0,2.52.8h2.52V3.83h-2.47a3.16,3.16,0,0,0-2.67.9,5.53,5.53,0,0,0-.65,3.14v7.72Z" transform="translate(0 0)"/><path d="M147.38,41l-7.25-20.16V41h-3.82V0h3.82V17.67l7-17.67h4.13l-7.85,18.67L151.76,41h-4.38Z" transform="translate(0 0)"/><path d="M153.76,41V0h3.37V21.76q0,4-.05,8.26l6.34-30H168V41h-3.32V20.16q0-4.43.05-9.11L157.58,41h-3.82Z" transform="translate(0 0)"/></svg>
					<a href="#callback" class="btn">стать арендатором</a>
					<div class="points dis-fl fl-rwwr jsc-ct alit-ct">
						<div class="item">
							<p class="desc">7 <span>минут ходьбы от метро</span></p>
						</div>
						<div class="item">
							<p class="desc">6 <span>этажей</span></p>
						</div>
						<div class="item">
							<p class="desc">124 <span>торговые площади</span></p>
						</div>
						<div class="item">
							<p class="desc">100+ <span>парковочных мест</span></p>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 2ND BLOCK -->
			<section class="sect sect-about" id="about">
				<div class="content dis-fl fl-rwwr jsc-spbet alit-ct">
					<div class="img" data-animate="fadeInLeft" data-duration="1.25s" data-delay="0s" data-offset="25" data-iteration="1">
						<div>
							<img src="img/vesh_01.jpg" alt="">
						</div>
					</div>
					<h2>О нас</h2>
					<div class="desc">
						<p>Район «Вешняки» это геометрически выверенный треугольник, место жительства более 120 тысяч москвичей.</p>
						<p>ТЦ «Вешняки» — самый крупный торговый объект этого треугольника, расположенный в центре жилых массивов. И жители посещают флагманский маркет минимум раз в неделю. Ведь здесь на шести этажах класса А есть почти всё, что нужно для полноценной жизни.</p>
						<p>Мы гордимся своей ролью центра притяжения района и делаем всё, чтобы главный магазин «Вешняков» был всегда актуальным и современным — лучшим для посетителей!</p>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 3RD BLOCK -->
			<section class="sect sect-arenda" id="arenda">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title">
						<h3>арендаторы</h3>
						<hr>
						<p class="subtitle">Всего представлено более 70 категорий организаций!</p>
					</div>
					<div class="logos-wall dis-grid">
						<div class="item">
							<div class="inner">
								<img src="img/01/1-1.png" alt="">
							</div>
							<div class="outer">
								<img src="img/new-logo/milavitsa.jpeg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-2.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-2.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-3.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-3.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-4.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-4.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-5.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/new-logo/tvoe.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-6.jpg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-6.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-7.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-7.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-8.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-8.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/new-logo/vkusno.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-9.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-10.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-10.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-11.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-11.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-12.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-12.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-13.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-13.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-14.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-14.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/new-logo/homecredit.jpg" alt="">
							</div>
							<div class="outer">
								<img src="img/new-logo/milavitsa.jpeg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-3.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-6.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-8.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-4.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-5.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/new-logo/vkusno.png" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-10.svg" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-8.svg" alt="">
							</div>
						</div>
						<div class="item">
							<div class="inner">
								<img src="img/01/1-2.png" alt="">
							</div>
							<div class="outer">
								<img src="img/02/2-7.png" alt="">
							</div>
						</div>
						<div class="new-logo">
							<div class="item">
								<div class="inner">
									<img src="img/new-logo/bishmak.jpg" alt="">
								</div>
								<div class="outer">
									<img src="img/new-logo/krutishka.png" alt="">
								</div>
							</div>													
							<div class="item">
								<div class="inner">
									<img src="img/new-logo/ozon.png" alt="">
								</div>
								<div class="outer">
									<img src="img/new-logo/wb.png" alt="">
								</div>
							</div>
							
							<div class="item">
								<div class="inner">
									<img src="img/new-logo/yandex-market.png" alt="">
								</div>
								<div class="outer">
									<img src="img/new-logo/redmond.jpg" alt="">
								</div>
							</div>
							
							<div class="item">
								<div class="inner">
									<img src="img/new-logo/hottur.jpeg" alt="">
								</div>
								<div class="outer">
									<img src="img/new-logo/1001tur.jpg" alt="">
								</div>
							</div>
							
							<div class="item">
								<div class="inner">
									<img src="img/new-logo/podrushka.jpg" alt="">
								</div>
								<div class="outer">
									<img src="img/new-logo/redmond.jpg" alt="">
								</div>
							</div>						
						
						</div>
						
					</div>
					<button class="show-more" type="button">Смотреть больше</button>
				</div>
			</section>
			<!-- ********* -->
			<!-- 4TH BLOCK -->
			<section class="sect sect-testimonials" id="testimonials">
				<div class="content dis-fl fl-colnw jsc-flst alit-ct">
					<div class="title">
						<h2>
							<svg xmlns="http://www.w3.org/2000/svg" contentEditable class="ttl">
								<defs>
									<linearGradient id="textgradient_1" x1="0%" x2="100%" y1="50%" y2="50%">
										<stop stop-color = "#00f0ff" offset = "0%"/>
										<stop stop-color = "#fa00ff" offset = "100%"/>
									</linearGradient>
								</defs>
								<text x="0%" y="90%" fill="url(#textgradient_1)">Вглядитесь в детали!</text>
							</svg>
						</h2>
					</div>
					<div class="video-block dis-fl fl-colnw jsc-ct alit-ct">
						<?php include 'img/video_arrow.svg' ?>
						<!-- <img src="img/video_arrow.svg" alt=""> -->
						<video class="video" width="100%" playsinline muted loop>
							<source src="video/video.mp4" type="video/mp4">
						</video>
						<div class="play" role="button"></div>
						<div class="volume" role="button"></div>
					</div>
					<div class="title">
						<h2>
							<svg xmlns="http://www.w3.org/2000/svg" contentEditable>
								<defs>
									<linearGradient id="textgradient_1" x1="0%" x2="100%" y1="50%" y2="50%">
										<stop stop-color = "#2dc4ff" offset = "0%"/>
										<stop stop-color = "#797cff" offset = "100%"/>
									</linearGradient>
									<linearGradient id="textgradient_2" x1="0%" x2="100%" y1="50%" y2="50%">
										<stop stop-color = "#48abff" offset = "0%"/>
										<stop stop-color = "#d821ff" offset = "100%"/>
									</linearGradient>
								</defs>
								<text x="0%" y="42%" fill="url(#textgradient_1)">нас</text>
								<text x="8%" y="92%" fill="url(#textgradient_2)">любят!</text>
							</svg>
						</h2>
						<p>И это именно тот случай, когда у сильных эмоций есть рациональные причины — ведь мы вкладываем душу, чтобы все наши гости не только остались довольны, но и захотели вернуться.</p>
					</div>
					<div class="testimonials-wall dis-fl fl-rwwr jsc-spbet alit-flst">
						<div class="item">
							<img src="img/testimonials-1.svg" alt="">
							<p class="desc">Cамое комфортное и современное торговое здание района. Проект реконструкции здания сделал известный архитектор Борис Олегович Уборевич-Боровский.</p>
						</div>
						<div class="item">
							<img src="img/testimonials-2.svg" alt="">
							<p class="desc">Легендарный советский районный универмаг, который знают все местные жители с молодости или детства.</p>
						</div>
						<div class="item">
							<img src="img/testimonials-3.svg" alt="">
							<p class="desc">Быстрая заполняемость арендаторами после открытия; проходное место, высокий трафик.</p>
						</div>
						<div class="item">
							<img src="img/testimonials-4.svg" alt="">
							<p class="desc">Благодаря выгодному расположению захватывается широкая аудитория посетителей всех возрастов.</p>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 5TH BLOCK -->
			<section class="sect sect-wwd" id="wwd">
				<div class="content dis-fl fl-colnw jsc-flst alit-ct">
					<div class="item item-left dis-fl fl-rwwr jsc-spbet alit-ct">
						<div class="title">
							<h3 >мы одеваем</h3>
							<hr>
							<p class="desc">Кроме демократической Familia, ещё 14 магазинов одежды на любой вкус и бюджет. И это ещё не считая джинсовые магазины, спецодежду, одежду больших размеров и для будущих мам.</p>
						</div>
						<div class="images" data-animate="fadeInRight" data-duration="1.25s" data-delay="0s" data-offset="25" data-iteration="1">
							<div class="img">
								<img src="img/wwd-1-1.jpg" alt="">
							</div>
							<div class="img">
								<img src="img/wwd-1-2.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item item-right dis-fl fl-rwwr jsc-spbet alit-ct">
						<div class="title">
							<h3>мы развиваем и украшаем</h3>
							<hr>
							<p class="desc">Детская тема, кроме одежды и игрушек, представлена игровой площадкой, товарами для творчества и рукоделия, школой танцев и искусств. А мамы идут сюда ради салона красоты, магазинов парфюмерии и косметики, ювелирных украшений и сделать ноготочки.</p>
						</div>
						<div class="images" data-animate="fadeInLeft" data-duration="1.25s" data-delay="0s" data-offset="25" data-iteration="1">
							<div class="img">
								<img src="img/wwd-2-1.jpg" alt="">
							</div>
							<div class="img">
								<img src="img/wwd-2-2.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item item-left dis-fl fl-rwwr jsc-spbet alit-ct">
						<div class="title">
							<h3>у нас всё, что надо</h3>
							<hr>
							<p class="desc">Как говорят сами жители «Вешняков», наш ТЦ привлекает их как «Отличное место для того, чтобы развеяться и выбраться куда-то с семьёй, здесь есть всё для досуга и закупки». Не выходя из здания, можно найти всё, что надо. И мы вас со всем радушием ждём!</p>
						</div>
						<div class="images" data-animate="fadeInRight" data-duration="1.25s" data-delay="0s" data-offset="25" data-iteration="1">
							<div class="img">
								<img src="img/wwd-3-1.jpg" alt="">
							</div>
							<div class="img">
								<img src="img/wwd-3-2.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item item-right dis-fl fl-rwwr jsc-spbet alit-ct">
						<div class="title">
							<h3>мы удивляем</h3>
							<hr>
							<p class="desc">Вам нужно что-то необычное, сюрприз для ребёнка или романтический подарок для любимого человек, но вы пока сами не знаете, что конкретно? Просто пройдите по нашим этажам, среди огромного количества магазинов, магазинчиков и островов, вы обязательно встретите что-то особое, подходящее именно вашем случаю!</p>
						</div>
						<div class="images" data-animate="fadeInLeft" data-duration="1.25s" data-delay="0s" data-offset="25" data-iteration="1">
							<div class="img">
								<img src="img/wwd-4-1.jpg" alt="">
							</div>
							<div class="img">
								<img src="img/wwd-4-2.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 6TH BLOCK -->
			<section class="sect sect-fcort" id="fcort">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="logos dis-fl fl-rwwr jsc-spbet alit-ct" data-animate="fadeInUp" data-duration="1.25s" data-delay="0.5s" data-offset="50" data-iteration="1">
						<div class="item">
							<img src="img/fcort/bk_white.svg" alt="">
						</div>
						<div class="item">
							<img src="img/fcort/teremok.svg" alt="">
						</div>
						<div class="item">
							<img src="img/fcort/kfc_white.svg" alt="">
						</div>
						<div class="item">
							<img src="img/new-logo/vkusno_white.svg" alt="">
						</div>
						<div class="item">
							<img src="img/fcort/randw_white.svg" alt="">
						</div>
					</div>
					<div class="title">
						<h2>гoлодны?</h2>
					</div>
					<p class="desc desc-1">Проголодались, или решили просто посидеть — на четвёртом этаже расположен внушительный фудкорт с большим выбором мест и ресторанных концепций. Здесь любят проводит время и семьи, а молодёжь массово «чиллит», перекусывая едой из McDonald’s, Burger King и KFC.</p>
					<p class="desc desc-2">Проголодались, или решили просто<br>посидеть — на четвёртом этаже<br>расположен внушительный<br>фудкорт с большим выбором<br>мест и ресторанных концепций.<br>Здесь любят проводит<br>время и семьи,<br>а молодёжь массово<br>«чиллит»,<br>перекусывая едой<br>из McDonald’s,<br>Burger King и KFC.</p>
				</div>
			</section>
			<!-- ********* -->
			<!-- 7TH BLOCK -->
			<section class="sect sect-plan" id="plan">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title">
						<h3>план этажей</h3>
					</div>
					<div class="slider" id="slider-plan">
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-1.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
									<a href="img/plan/floor-01_01.jpg" data-fancybox="1_floor">
										<img src="img/plan/floor-01_01.jpg" alt="">
									</a>
									<a href="img/plan/floor-01_02.jpg" data-fancybox="1_floor">
										<img src="img/plan/floor-01_02.jpg" alt="">
									</a>
									<a href="img/plan/floor-01_03.jpg" data-fancybox="1_floor">
										<img src="img/plan/floor-01_03.jpg" alt="">
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-2-28-02-2023.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
									<a href="img/plan/floor-02_01.jpg" data-fancybox="2_floor">
										<img src="img/plan/floor-02_01.jpg" alt="">
									</a>
									<a href="img/plan/floor-02_02.jpg" data-fancybox="2_floor">
										<img src="img/plan/floor-02_02.jpg" alt="">
									</a>
									<a href="img/plan/floor-02_03.jpg" data-fancybox="2_floor">
										<img src="img/plan/floor-02_03.jpg" alt="">
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-3.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
									<a href="img/plan/floor-03_01.jpg" data-fancybox="3_floor">
										<img src="img/plan/floor-03_01.jpg" alt="">
									</a>
									<a href="img/plan/floor-03_02.jpg" data-fancybox="3_floor">
										<img src="img/plan/floor-03_02.jpg" alt="">
									</a>
									<a href="img/plan/floor-03_03.jpg" data-fancybox="3_floor">
										<img src="img/plan/floor-03_03.jpg" alt="">
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-4.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
									<a href="img/plan/floor-04_01.jpg" data-fancybox="4_floor">
										<img src="img/plan/floor-04_01.jpg" alt="">
									</a>
									<a href="img/plan/floor-04_02.jpg" data-fancybox="4_floor">
										<img src="img/plan/floor-04_02.jpg" alt="">
									</a>
									<a href="img/plan/floor-04_03.jpg" data-fancybox="4_floor">
										<img src="img/plan/floor-04_03.jpg" alt="">
									</a>
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-5.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
								</div>
							</div>
						</div>
						<div class="carousel-cell">
							<div class="cell dis-fl fl-rwwr jsc-spbet alit-ct">
								<img src="img/plan-6.png" alt="">
								<div class="images dis-fl fl-colnw jsc-ct alit-ct">
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- ********* -->
			<!-- 8TH BLOCK -->
			<section class="sect sect-callback" id="callback">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title">
						<h3>хотите к нам?</h3>
						<br>
						<div class="text">
							<span class="texttype"></span>
						</div>
					</div>
					<form action="" class="post-form" id="form-callback" name="form-callback" method="post" enctype="multipart/form-data" accept-charset="UTF-8" onsubmit="return valForm(this)">
						<div class="dis-fl fl-rwwr jsc-spbet alit-flst">
							<div class="input">
								<span>Компания / Бренд</span>
								<input type="text" id="company" placeholder="" name="company" required="">
							</div>
							<div class="input">
								<span>Имя</span>
								<input type="text" id="name" placeholder="" name="imya" required="">
							</div>
							<div class="input">
								<span>Телефон</span>
								<input type="tel" id="phone" placeholder="" name="phone" required="">
							</div>
							<div class="input">
								<span>Email</span>
								<input type="email" id="email" placeholder="" name="email" required="">
							</div>
						</div>
						<button class="btn btn-form" type="submit">отправить</button>
					</form>
				</div>
			</section>
			<!-- ********* -->
			<!-- 9TH BLOCK -->
			<section class="sect sect-insta" id="insta">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title">
						<h2>veshnyaki</h2>
					</div>
				</div>
				<div class="slider" id="slider-insta"></div>
				<div class="content dis-fl fl-rwwr jsc-flst alit-ct">
					<a href="https://www.instagram.com/tc_veshnyaki2018/?hl=ru" class="btn">подпишись</a>
					<p class="desc">И будь всегда  в курсе акций, скидок и наших мероприятий!</p>
				</div>
			</section>
			<!-- ********* -->
			<!-- FOOTER -->
			<footer class="gal" id="contacts">
				<div class="map" id="map"></div>
				<div class="container dis-fl fl-rwwr jsc-spbet alit-ct">
					<div class="address">
						<p>Адрес <span>г. Москва,<br>Вешняковская улица, 18</span></p>
					</div>
					<div class="phone">
						<p>По вопросам аренды <a href="tel:<?=$config['phone_link']?>"><?=$config['phone_text']?></a></p>
						<img src="../img/footer-arrow.svg" alt="" id="fimg-1">
						<img src="../img/footer-arrow-2.svg" alt="" id="fimg-2">
					</div>
					<div class="whours">
						<p>Часы работы <span>С 10.00 до 23.00</span></p>
					</div>
				</div>
			</footer>
		</main>
		<!-- styles -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css">
		<!-- scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="js/jquery.scrolla.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/jquery.maskedinput.js"></script>
		<script src="https://polyfill.io/v3/polyfill.min.js?features=Promise"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/1.1.4/typed.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/nanogram.js@2.0.0/dist/nanogram.iife.min.js"></script>
		<script src="js/jquery.fancybox.min.js"></script>
		<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
		<script src="js/map.js?v=<?php echo filectime('js/map.js'); ?>"></script>
		<script src="js/custom.js?v=<?php echo filectime('js/custom.js'); ?>"></script>
        <script src="js/form.js"></script>

    <?/*<script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>*/?>
    </body>
</html>