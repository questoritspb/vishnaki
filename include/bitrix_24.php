<?php

$name = trim($_POST['imya']);
$company = trim($_POST['company']);
$phone = trim($_POST['phone']);
$email = trim($_POST['email']);
$comment = '';
$source_id = 'WEB';

$queryUrl = 'https://tcomp.bitrix24.ru/rest/339/asdatel3bxsrjq1v/crm.lead.add.json';
$user_id_assigned = 217;

// формируем параметры для создания лида в переменной $queryData // Добавить лид → добавить сделку deal.add
$queryData = http_build_query([
    'fields' => array(
        'TITLE'   => $name,
        'NAME'   => $name,
        'PHONE' => [
            'n0' => [
                'VALUE' => $phone,
                'VALUE_TYPE' => 'WORK'
            ]
        ],
        'EMAIL' => [
            'n0' => [
                'VALUE' => $email,
                'VALUE_TYPE' => 'WORK'
            ]
        ],
        //'ASSIGNED_BY_ID' => $user_id_assigned,
        'COMPANY_TITLE' => $company,
        'SOURCE_ID' => (empty($source_id) ? 1 : $source_id),
        'SOURCE_DESCRIPTION' => (empty($source_description) ? "Сайт: {$_SERVER['HTTP_HOST']}" : $source_description),
        'COMMENTS' => $comment
        /*'TRACE' => $trace,
        'UTM_SOURCE' => (empty($_COOKIE['qv_utm_source']) ? $utm_source : $_COOKIE['qv_utm_source']),
        'UTM_CAMPAIGN' => (empty($_COOKIE['qv_utm_campaign']) ? $utm_campaign : $_COOKIE['qv_utm_campaign']),
        'UTM_CONTENT' => (empty($_COOKIE['qv_utm_content']) ? $utm_content : $_COOKIE['qv_utm_content']),
        'UTM_MEDIUM' => (empty($_COOKIE['qv_utm_medium']) ? $utm_medium : $_COOKIE['qv_utm_medium']),
        'UTM_TERM' => (empty($_COOKIE['qv_utm_term']) ? $utm_term : $_COOKIE['qv_utm_term'])*/

    ),
    'params' => array("REGISTER_SONET_EVENT" => "Y")
]);
// обращаемся к Битрикс24 при помощи функции curl_exec
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));
$result = curl_exec($curl);
curl_close($curl);

/**/