<?php
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
   !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
   strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    $months = [
        'январь',
        'февраль',
        'март',
        'апрель',
        'май',
        'июнь',
        'июль',
        'август',
        'сентябрь',
        'октябрь',
        'ноябрь',
        'декабрь'
    ];
    $month = date('n')-1;

    $email_to = ['solovyevantony@gmail.com', 'ifuflygin@gmail.com', '79057567141@yandex.ru', 'kozlova.o@tcveshnyaki.ru', 'korgina.v@tcveshnyaki.ru', 'kozyrev.d@tcveshnyaki.ru'];
//    $email_to = ['kmv@questor.ru'];
    $email_from = 'postmaster@tc-veshnyaki.ru';


    $subject = "Новая заявка на аренду с сайта " . $_SERVER['HTTP_HOST'];
    $headers = [
        'From' => $email_from,
        'Reply-To' => $email_from,
        'Content-Type' => 'text/html;charset=utf-8'
    ];
    $message = "
     <b>Вам пришла новая заявка на аренду помещений в ТЦ Вешняки</b>
     <br>
     <br>    
        Время и дата заявки: " . date('d') . " " . $months[$month] . " " .date("Y") . " в " . date("H:i") . "
     <br>
     <br>   
    Компания / Бренд: {$_POST['company']}<br>
    Имя: {$_POST['imya']}<br>
    Телефон: {$_POST['phone']}<br>
    Email: {$_POST['email']}<br>
<br>
    ";
    /*
    <br>
    <br>
    Просьба менеджеру подготовить в ответ следующую информацию:<br>
    <br>
    1. ФИО арендатора –<br>
    2. Контактный телефон — <br>
    3. Название компании —<br>
    4. Вид деятельности — <br>
    5. Интересующий формат площади —<br>
    6. Размер площади — <br>
    7. Период аренды — <br>
    8. Особенные условия —<br>
    <br>
    9. Был ли в ТЦ Вешняки —<br>
    <br>
    Готовность к аренде по итогам разговора по оценке менеджера (низкая / средняя / высокая) —<br>
    <br>
    В случае отказа по итогам разговора указать причину:<br>
    <br>
    (Подпись менеджера)<br>
     * */
    foreach ($email_to as $mail) {
        mail($mail, $subject, $message, $headers);
    }
    include 'bitrix_24.php';
    echo json_encode(['result' => ['answer' => 'success', "message" => ""]]);
    /*if (mail($email_to, $subject, $message, $headers)) {
        echo json_encode(['result' => ['answer' => 'success', "message" => ""]]);
    } else {
        echo json_encode(['result' => ['answer' => 'error', "message" => ""]]);
    }*/
}
