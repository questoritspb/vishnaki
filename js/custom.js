// new
/*
 * HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
 */
(function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";
c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);
if(g)return a.createDocumentFragment();for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);

/*-------------------*/
/*-------------------*/
// custom scripts
/*-------------------*/
/*-------------------*/
// toggle hamburger & collapse nav
$('.menu-toggle').click(function() {
	$('.site-nav').toggleClass('site-nav--open');
	$('a.logo svg').toggleClass('menu-open');
	$(this).toggleClass('open');
});
/*-------------------*/
/*-------------------*/
$(document).ready(function() {
	var viewportWidth = $(window).width();
	// sticky header
	if (viewportWidth > 760) {
		(function() {
			var doc = document.documentElement;
			var w = window;
			/*
			define four variables: curScroll, prevScroll, curDirection, prevDirection
			*/
			var curScroll;
			var prevScroll = w.scrollY || doc.scrollTop;
			var curDirection = 0;
			var prevDirection = 0;
			var header = document.getElementsByTagName('header');
			var toggled;
			var threshold = 200;
			var header_h = $('header').outerHeight();
			var checkScroll = function() {
				curScroll = w.scrollY || doc.scrollTop;
				if (curScroll > prevScroll) {
					// scrolled down
					curDirection = 2;
				} else {
					//scrolled up
					curDirection = 1;
				}
				if (curDirection !== prevDirection) {
					toggled = toggleHeader();
				}
				prevScroll = curScroll;
				if (toggled) {
					prevDirection = curDirection;
				}
			};
			var toggleHeader = function() { 
				toggled = true;
				if (curDirection === 2 && curScroll > threshold) {
					$(header).addClass('hide').css('top', 0 - header_h);
				} else if (curDirection === 1) {
					$(header).removeClass('hide').css('top', 0);
				} else {
					toggled = false;
				}
				return toggled;
			};
			window.addEventListener('scroll', checkScroll);
		})();
	}
	// smooth scroll to div
	$('a[href^="#"]').on('click',function (e) {
		e.preventDefault();
		var target = this.hash,
		$target = $(target);
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 500, 'swing', function () {
			window.location.hash = target;
		});
	});
	// close mobile menu after click on a
	$('nav.site-nav ul li a').click(function() {
		if (viewportWidth < 780) {
			$('.site-nav').toggleClass('site-nav--open');
			$('a.logo svg').toggleClass('menu-open');
			$('.menu-toggle').toggleClass('open');
		}
	});
	// slider 1
	$('#slider-plan').slick({
		slide: '.carousel-cell',
		centerMode: false,
		infinite: false,
		dots: true,
		arrows: false,
		speed: 600
	});
	$('#slider-plan').on('afterChange', function(slick, direction) {
		var cch = $('#slider-plan .slick-list').height();
		// console.log(cch);
		$('#slider-plan .carousel-cell').height(cch);
	});
	// instagram
	var lib = new Nanogram();
	function buildPorfolio() {
		return lib.getMediaByUsername('tc_veshnyaki2018').then(function(response) {
			if (console.table) {
				// console.table(response.profile);
			}
			// Get photos
			var photos = response.profile.edge_owner_to_timeline_media.edges;
			var items = [];
			// Create new elements
			for (var i = 0; i <= photos.length - 1; i++) {
				var current = photos[i].node;
				var div = document.createElement('div');
				var link = document.createElement('a');
				var img = document.createElement('img');
				var thumbnail = current.thumbnail_resources[4];
				var imgSrc = thumbnail.src;
				var imgWidth = thumbnail.config_width;
				var imgHeight = thumbnail.config_height;
				var imgAlt = current.accessibility_caption;
				var shortcode = current.shortcode;
				var linkHref = 'https://www.instagram.com/p/' + shortcode;
				div.classList.add('carousel-cell');
				img.classList.add('carousel-img');
				img.src = imgSrc;
				// img.width = imgWidth;
				// img.height = imgHeight;
				img.alt = imgAlt;
				link.classList.add('carousel-link');
				link.href = linkHref;
				link.target = '_blank';
				link.appendChild(img);
				div.appendChild(link);
				items.push(div);
			}
			// Create container for our portfolio
			var container = $('#slider-insta');
			// Append all photos to our container
			for (var j = 0; j <= items.length - 1; j++) {
				container.append(items[j]);
			}
			// append to slider 2
			container.slick({
				slide: '.carousel-cell',
				centerMode: true,
				centerPadding: '1.5rem',
				slidesToShow: 6,
				infinite: true,
				dots: false,
				arrows: false,
				autoplay: true,
				autoplaySpeed: 6000,
				speed: 600,
				variableWidth: true,
				responsive: [
					{
						breakpoint: 600,
						settings: {
							infinite: false,
							centerMode: false,
							autoplay: true,
							slidesToShow: 1
						}
					}
				]
			});
			var wdt = $('.slick-list').width();
			if (viewportWidth < 540) {
				$('#slider-insta .carousel-cell').height(wdt).width(wdt);
			}
		});
	}
	buildPorfolio();
	// show more button
	$('.show-more').click(function() {
		if ($(this).hasClass('active')) {
			$('.logos-wall .item:nth-child(n+7):nth-child(-n+15)').slideUp('300');
			$(this).removeClass('active').text('Смотреть больше');
		} else {
			$('.logos-wall .item:nth-child(n+7):nth-child(-n+15)').slideDown('300');
			$(this).addClass('active').text('скрыть');
		}
	});
	// typing text
	var commands = [["Вместе каждый из нас будет успешнее, <b>оставьте заявку</b> на аренду!"]];
	var current_command = null;
	function type_command() {
		if (current_command === null) {
			current_command = 0;
		} else {
			current_command++;
			if (current_command > commands.length - 1) {
				current_command = 0;
			}
		}
		var command = commands[current_command];
		$(".texttype").typed({
			strings: [command[0]],
			typeSpeed: 4,
			backDelay: 1500,
			startDelay: 2500,
			loop: false,
			contentType: 'html',
			callback: function() {
				setTimeout(type_command, 25000);
			}
		});
	}
	type_command();
	// callback click
	$('#callback').click(function() {
		$('.text').addClass('clicked');
		$('form.post-form').slideDown('300');
	});
	// fancybox
});
/*-------------------*/
/*-------------------*/
// animation on scroll
$('[data-animate]').scrolla({
	mobile: true,
	once: false,
	animateCssVersion: 4
});
/*-------------------*/
/*-------------------*/
// maskedinput
jQuery(function($) {
	$("input[name='phone']").mask("+7(999)999-99-99");
});
/*-------------------*/
/*-------------------*/
// video-player
// Plugin isInViewport
!function(e,n){"object"==typeof exports&&"undefined"!=typeof module?n(require("jquery"),require("window")):"function"==typeof define&&define.amd?define("isInViewport",["jquery","window"],n):n(e.$,e.window)}(this,function(e,n){"use strict";function t(n){var t=this;if(1===arguments.length&&"function"==typeof n&&(n=[n]),!(n instanceof Array))throw new SyntaxError("isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions");return n.forEach(function(n){"function"!=typeof n?(console.warn("isInViewport: Argument(s) passed to .do/.run should be a function or an array of functions"),console.warn("isInViewport: Ignoring non-function values in array and moving on")):[].slice.call(t).forEach(function(t){return n.call(e(t))})}),this}function o(n){var t=e("<div></div>").css({width:"100%"});n.append(t);var o=n.width()-t.width();return t.remove(),o}function r(t,i){var a=t.getBoundingClientRect(),u=a.top,c=a.bottom,f=a.left,l=a.right,d=e.extend({tolerance:0,viewport:n},i),s=!1,p=d.viewport.jquery?d.viewport:e(d.viewport);p.length||(console.warn("isInViewport: The viewport selector you have provided matches no element on page."),console.warn("isInViewport: Defaulting to viewport as window"),p=e(n));var w=p.height(),h=p.width(),v=p[0].toString();if(p[0]!==n&&"[object Window]"!==v&&"[object DOMWindow]"!==v){var g=p[0].getBoundingClientRect();u-=g.top,c-=g.top,f-=g.left,l-=g.left,r.scrollBarWidth=r.scrollBarWidth||o(p),h-=r.scrollBarWidth}return d.tolerance=~~Math.round(parseFloat(d.tolerance)),d.tolerance<0&&(d.tolerance=w+d.tolerance),l<=0||f>=h?s:s=d.tolerance?u<=d.tolerance&&c>=d.tolerance:c>0&&u<=w}function i(n){if(n){var t=n.split(",");return 1===t.length&&isNaN(t[0])&&(t[1]=t[0],t[0]=void 0),{tolerance:t[0]?t[0].trim():void 0,viewport:t[1]?e(t[1].trim()):void 0}}return{}}e="default"in e?e.default:e,n="default"in n?n.default:n,/**
 * @author  Mudit Ameta
 * @license https://github.com/zeusdeux/isInViewport/blob/master/license.md MIT
 */
e.extend(e.expr[":"],{"in-viewport":e.expr.createPseudo?e.expr.createPseudo(function(e){return function(n){return r(n,i(e))}}):function(e,n,t){return r(e,i(t[3]))}}),e.fn.isInViewport=function(e){return this.filter(function(n,t){return r(t,e)})},e.fn.run=t});
//# isInViewport
// Play Video
var videoEl = $('video')[0];
var playBtn = $('.play');
var volumeControl = $('.volume');
$(function() {
	var $video = $('.video');
	var $window = $(window);
	$window.scroll(function() {
		if ($video.is(":in-viewport")) {
			$video[0].play();
			playBtn.addClass('active');
		} else {
			$video[0].pause();
			playBtn.removeClass('active');
		}
	});
});
playBtn.click(function() {
	if (videoEl.paused) {
		videoEl.play();
		playBtn.addClass('active');
	} else {
		videoEl.pause();
		playBtn.removeClass('active');
	}
});
volumeControl.click(function() {
	if ($(this).hasClass('active')) {
		videoEl.muted = 1;
		videoEl.volume = 0.0;
		$(this).removeClass('active');
	} else {
		videoEl.muted = 0;
		videoEl.volume = 0.15;
		$(this).addClass('active');
	}
});

document.addEventListener('DOMContentLoaded', function () {
	document.querySelector('.new-year-popup .close').addEventListener('click', function () {
		document.querySelector('.new-year-popup').style.display = 'none';
	});
});