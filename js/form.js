$(document).ready(function () {
    $(document).on('submit', '#form-callback', function (e) {
        e.preventDefault;
        var form = $(this);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/include/send.php',
            data: form.serialize(),
            success: function (respond) {
                if (respond.result.answer == 'error') {
                    console.log('Error ajax query: ' + respond.result.message);
                } else if (respond.result.answer == 'success') {
                    $('#form-callback > div').html('<div class="alert alert-info">Сообщение успешно отправлено. В' +
                        ' ближайшее время с Вами свяжется наш менеджер.</div>')
                } else {
                    console.log('Error ajax query');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Ajax error: ' + textStatus);
            }
        });

        return false;
    });


});