// new
/*
 * HTML5 Shiv v3.7.0 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
 */
(function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";
c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);
if(g)return a.createDocumentFragment();for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);

/*-------------------*/
/*-------------------*/
// custom scripts
/*-------------------*/
/*-------------------*/
// toggle hamburger & collapse nav
$('.menu-toggle').click(function() {
	$('.site-nav').toggleClass('site-nav--open');
	$('a.logo svg').toggleClass('menu-open');
	$(this).toggleClass('open');
});
/*-------------------*/
/*-------------------*/
$(document).ready(function() {
	var viewportWidth = $(window).width();
	// sticky header
	if (viewportWidth > 760) {
		(function() {
			var doc = document.documentElement;
			var w = window;
			/*
			define four variables: curScroll, prevScroll, curDirection, prevDirection
			*/
			var curScroll;
			var prevScroll = w.scrollY || doc.scrollTop;
			var curDirection = 0;
			var prevDirection = 0;
			var header = document.getElementsByTagName('header');
			var toggled;
			var threshold = 200;
			var header_h = $('header').outerHeight();
			var checkScroll = function() {
				curScroll = w.scrollY || doc.scrollTop;
				if (curScroll > prevScroll) {
					// scrolled down
					curDirection = 2;
				} else {
					//scrolled up
					curDirection = 1;
				}
				if (curDirection !== prevDirection) {
					toggled = toggleHeader();
				}
				prevScroll = curScroll;
				if (toggled) {
					prevDirection = curDirection;
				}
			};
			var toggleHeader = function() { 
				toggled = true;
				if (curDirection === 2 && curScroll > threshold) {
					$(header).addClass('hide').css('top', 0 - header_h);
				} else if (curDirection === 1) {
					$(header).removeClass('hide').css('top', 0);
				} else {
					toggled = false;
				}
				return toggled;
			};
			window.addEventListener('scroll', checkScroll);
		})();
	}
	// close mobile menu after click on a
	// console.log(viewportWidth);
	$('nav.site-nav ul li a').click(function() {
		if (viewportWidth < 780) {
			$('.site-nav').toggleClass('site-nav--open');
			$('a.logo svg').toggleClass('menu-open');
			$('.menu-toggle').toggleClass('open');
		}
	});
	// 1 section top position
	var header = $('header').outerHeight();
	$('#gallery').css('margin-top', header);
	// img obj-fit polyfill for ie11
	$(function() {
		objectFitImages()
	});
	// fancybox with slider
	$('a[data-fancybox]').attr('data-index', function(arr) {
		return arr;
	});
	if (viewportWidth <= 480) {
		$('.sect-gallery a').removeAttr('data-src data-type');
		$('.sect-gallery a').each(function() {
			var img_src = $(this).find('figure > img').attr('src');
			console.log(img_src);
			$(this).attr('href', img_src).attr('data-fancybox', 'gallery');;
		});
		$('a[data-fancybox]').fancybox({
			infobar: true,
			buttons: [
				"zoom",
				//"share",
				// "slideShow",
				//"fullScreen",
				//"download",
				// "thumbs",
				"close"
			]
		});
	} else if (viewportWidth > 480) {
		$('a[data-fancybox]').fancybox({
			afterLoad: function(instance, slide) {
				$('#slider-gallery').slick({
					slide: '.carousel-cell',
					adaptiveHeight: true,
					centerMode: false,
					infinite: false,
					dots: false,
					appendArrows: $('#arrows_2'),
					arrows: true,
					prevArrow: '<button type="button" class="slick-prev"></button>',
					nextArrow: '<button type="button" class="slick-next"></button>',
					speed: 600,
				});
				var wdt = $(window).height() - 200;
				console.log(wdt);
				$('#slider-gallery .carousel-cell .img img').css({
					'max-height': wdt
				});
				$('.btn.chng').click(function() {
					if($(this).hasClass('active')) {
						$('#slider-gallery').css('opacity', 0).slideDown('300').animate({
								opacity: 1
							}, {
								queue: false,
								duration: '300'
								}
						);
						$('#slider-plan-fancy').css('opacity', 1).slideUp('300').animate({
								opacity: 0
							}, {
								queue: false,
								duration: '300'
								}
						).slick('destroy');
						$(this).removeClass('active').text('смотреть схему этажей');
					} else {
						$('#slider-gallery').css('opacity', 1).slideUp('300').animate({
								opacity: 0
							}, {
								queue: false,
								duration: '300'
								}
						);
						$('#slider-plan-fancy').css('opacity', 0).slideDown('300').animate({
								opacity: 1
							}, {
								queue: false,
								duration: '300'
								}
						).slick({
							slide: '.carousel-cell',
							centerMode: false,
							infinite: false,
							dots: false,
							appendArrows: $('#arrows_1'),
							arrows: true,
							prevArrow: '<button type="button" class="slick-prev"></button>',
							nextArrow: '<button type="button" class="slick-next"></button>',
							speed: 600
						});
						$(this).addClass('active').text('вернуться к галерее');
					}
				});
			},
			afterShow: function(instance, slide) {
				var index = slide.opts.$orig.attr('data-index');
				var logo = slide.opts.$orig.find('.img img').attr('src');
				$('#slider-gallery').slick('slickGoTo', index);
				/*$('#slider-gallery').on('afterChange', function(event, slick, currentSlide) {
					// var csh = $(currentSlide).innerHeight();
					console.log(slick, currentSlide);
				})*/
				$('.container img').attr('src', logo);
			}
		});
	}
	// show-more button
	$('.show-more').click(function() {
		var timeout;
		if ($(this).hasClass('active')) {
			$('.gallery-wall.p3').slideUp('300');
			$(this).removeClass('active').text('Смотреть больше');
		} else {
			$('.gallery-wall.p3').slideDown('300');
			$(this).addClass('active').text('скрыть');
		}
	});
});
/*-------------------*/
/*-------------------*/
// maskedinput
jQuery(function($) {
	$("input[name='phone']").mask("+7(999)999-99-99");
});
/*-------------------*/
/*-------------------*/