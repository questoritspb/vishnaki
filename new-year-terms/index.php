<?
header("Location: https://tc-veshnyaki.ru/");exit;
include_once '../config.php';
?>
<!doctype html>
<html lang="ru" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Торговый центр Вешняки - Мероприятие «Новогодняя лотерея»</title>
		<meta name="og:description" content="ТЦ «Вешняки - Мероприятие «Новогодняя лотерея»»">
		<link rel="canonical" href="https://tc-veshnyaki.ru/new-year-terms/">
		<meta property="og:locale" content="ru_RU">
		<meta property="og:url" content="https://tc-veshnyaki.ru/new-year-terms/">
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="tc-veshnyaki.ru">
		<meta property="og:title" content="Торговый центр Вешняки - Мероприятие «Новогодняя лотерея»">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:image" content="../img/tw-card.jpg">
		<meta name="twitter:title" content="Торговый центр Вешняки - Мероприятие «Новогодняя лотерея»">
		<meta name="twitter:description" content="Торговый центр Вешняки - Мероприятие «Новогодняя лотерея»">
		<link rel="icon" href="../img/favicon.svg">
		<link rel="mask-icon" href="../img/mask-icon.svg" color="#9fc321">
		<link rel="icon" href="../img/favicon-32x32.png" sizes="32x32">
		<link rel="icon" href="../img/favicon-64x64.png" sizes="64x64">
		<link rel="icon" href="../img/favicon-192x192.png" sizes="192x192">
		<link rel="icon" href="../img/favicon-512x512.png" sizes="512x512">
		<link rel="apple-touch-icon" href="../img/apple-touch-icon.png">
		<meta name="msapplication-TileImage" content="../img/favicon-270x270.png">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="../css/main.css?v=<?php echo filectime('../css/main.css'); ?>">
        <link rel="stylesheet" href="../css/custom.css?v=<?php echo filectime('../css/custom.css'); ?>">
		<!-- livereload script / delete before deploy! -->
		<!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-TN4J7NH');</script>
        <!-- End Google Tag Manager -->
        <!-- calltouch -->
        <script type="text/javascript">
            (function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","3oggdpha");
        </script>
        <!-- calltouch -->
	</head>
	<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TN4J7NH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
		<!-- WRAP -->
		<main class="wrap-site dis-fl fl-colnw">
			<!-- ****** -->
			<!-- HEADER -->
			<header class="gal">
				<div class="container dis-fl fl-rwnw jsc-spbet alit-ct">
					<a href="/" class="logo">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 168 46"><path d="M4.63,41V3.83H0V0H13V3.83H8.4V41H4.63Z" transform="translate(0 0)"/><path d="M28,41H15.46V0h3.82V37.24h6V0H29.1V37.29h2.57V46H28V41Z" transform="translate(0 0)"/><path d="M50.87,7a3.77,3.77,0,0,0-.6-2.44,2.38,2.38,0,0,0-2-.7H45.53V18.32h2.82a2.41,2.41,0,0,0,1.91-.65,3,3,0,0,0,.6-2.09V7Zm0.15,18a4.28,4.28,0,0,0-.55-2.49,2.33,2.33,0,0,0-2.11-.8H45.53V37.24H48.3q2.72,0,2.72-3.19V24.94ZM41.71,41V0H48.3Q51.62,0,53,1.64T54.44,7v8.21q0,3.83-2.52,4.53A3.77,3.77,0,0,1,54,21.06a4.74,4.74,0,0,1,.91,3.24v9.76q0,3.63-1.66,5.28a6.81,6.81,0,0,1-5,1.64H41.71Z" transform="translate(0 0)"/><path d="M58.07,41V0H69V3.83H61.9V18.22h5V22h-5V37.24h7.4V41H58.07Z" transform="translate(0 0)"/><path d="M72.18,0H76V37.24h7.55V0h3.82V37.24h8.25V0h3.82V41H72.18V0Z" transform="translate(0 0)"/><path d="M112.1,41V22h-5.33V41h-3.82V0h3.82V18.27h5.33V0h3.83V41H112.1Z" transform="translate(0 0)"/><path d="M119.65,41q0.75-4.83,1.61-9.61t1.86-9.81q-3.07-1.54-3.07-6V7.37q0-4,1.76-5.68A7.71,7.71,0,0,1,127.25,0h6.29V41h-3.82V22.4H127.2a2.34,2.34,0,0,1-.45,0q-0.86,4.68-1.56,9.26T123.83,41h-4.18Zm4.28-25.39a3.8,3.8,0,0,0,.75,2.59,3.35,3.35,0,0,0,2.52.8h2.52V3.83h-2.47a3.16,3.16,0,0,0-2.67.9,5.53,5.53,0,0,0-.65,3.14v7.72Z" transform="translate(0 0)"/><path d="M147.38,41l-7.25-20.16V41h-3.82V0h3.82V17.67l7-17.67h4.13l-7.85,18.67L151.76,41h-4.38Z" transform="translate(0 0)"/><path d="M153.76,41V0h3.37V21.76q0,4-.05,8.26l6.34-30H168V41h-3.32V20.16q0-4.43.05-9.11L157.58,41h-3.82Z" transform="translate(0 0)"/></svg>
					</a>
					<nav class="site-nav">
						<ul class="dis-fl fl-rwnw jsc-flend alit-ct">
							<li class="mob">
								<a href="../#about">о нас</a>
							</li>
							<li class="mob">
								<a href="../#arenda">арендаторы</a>
							</li>
							<li class="mob">
								<a href="/gallery/" class="active">галерея</a>
							</li>
							<li class="mob">
								<a href="../#testimonials">нас любят</a>
							</li>
							<li class="mob">
								<a href="../#wwd">мы одеваем и не только</a>
							</li>
							<li class="mob">
								<a href="../#fcort">голодны?</a>
							</li>
							<li class="mob">
								<a href="../#plan">карта этажей</a>
							</li>
                            <li class="mob">
                                <a href="/vacant-areas/">Свободные площади</a>
                            </li>
							<li class="mob">
								<a href="../#contacts">контакты</a>
							</li>
							<li class="mob time">с 10.00 до 23.00</li>
							<li class="desk">
								<!-- !!! delete 'inactive' class before deploy !!! -->
								<a href="#" class="inactive">для арендаторов</a>
							</li>
							<li class="desk">
								<a href="../#plan">карта этажей</a>
							</li>
							<li class="desk">
								<a href="/gallery/" class="active">галерея</a>
							</li>
							<li class="desk">
								<a href="../#contacts">контакты</a>
							</li>
                            <li class="desk">
                                <a href="tel:<?=$config['phone_link']?>" class="phone"><?=$config['phone_text']?></a>
                            </li>
							<li class="desk">
								<a href="/vacant-areas/" class="hdr btn">стать арендатором</a>
							</li>
						</ul>
					</nav>
                    <a href="tel:<?=$config['phone_link']?>" class="header__mobile-phone">
                        <img src="/img/call.svg" alt="Позвонить">
                    </a>
					<div class="menu-toggle">
						<div class="hamburger"></div>
					</div>
				</div>
			</header>
			<!-- ******* -->
			<!-- CONTENT -->
			<!-- ********* -->
			<!-- 1ST BLOCK -->
			<section class="sect sect-text">
				<div class="content">
					<h1 class="title" style="margin-bottom:1rem;text-align:center">Cтимулирующая рекламная акция<br>Мероприятие «Новогодняя лотерея».</h1>

					<p>Сроки проведения Новогодней лотереи с 05 декабря 2022 года по 24 декабря 2022 года.</p>
					<p>Настоящий розыгрыш — это стимулирующая рекламная акция, не является лотереей и публичным конкурсом. Цель проведения – повышение интереса у посетителей, увеличение посещаемости торгового центра «ВЕШНЯКИ».</p>
					<p>Организатор Новогодней лотереи ООО «Дилижанс Ритейл групп Си» Д.У.
					ИНН 7721778544, ОГРН 1127747237420, юридический адрес: 111538, Москва г., Внутригородская территория муниципальный округ Вешняки, Вешняковская ул, дом 18, этаж 1, помещение 16, телефон: 84955104521.</p>

					<p>*Участником Новогодней лотереи могут стать граждане РФ, дееспособные лица, достигшие 18-летнего возраста, проживающие на территории РФ, совершившие в Торговом центре «ВЕШНЯКИ» по адресу г. Москва, ул. Вешняковская, д.18, покупки на сумму 2000,00 (две тысячи) рублей и более одним чеком в период с 05 декабря по 24 декабря 2022 года. В период с 05 декабря по 24 декабря 2022 года с 12:00 до 22:00 владельцу чека необходимо обменять чек(и) на купон(ы), заполнить контактные данные и опустить в барабан Лототрона», расположенного по адресу г. Москва, ул. Вешняковская, д.18 на 1 этаже «Информационная стойка».</p>
					<p>Номинал 1 (одного) купона равен одному чеку на сумму 2000,00 (две тысячи) рублей и более (одним чеком). </p>
					<p>Табак и табачная продукция, алкогольная продукция, а также лотерейные билеты не учитывается в сумме чека покупки для участия в Новогодней лотерее.</p>
					<p>В розыгрыше Новогодней лотереи участвуют 100 (сто) единиц призов, в том числе бытовая техника, сертификаты на услуги, утешительные призы, главный приз «Телевизор Яндекс диагональю 50” ». </p>
					<p>Призовой фонд сформирован за счет денежных средств Организатора. </p>
					<p>Выплата денежного эквивалента стоимости вещественных Призов или замена другими Призами не производится. Цвет, модель и иные свойства Призов определяются по усмотрению Организатора, и могут не совпадать с ожиданиями Участников, а также с изображениями, представленными в рекламных материалах. </p>
					<p>В случае, если на момент вручения Призов товар с характеристиками, указанными в настоящих Правилах, отсутствует, Организатор вправе заменить Приз на товар с аналогичными или похожими характеристиками.</p>
					<p>Розыгрыш и вручение призов в Новогодней лотерее состоится в день проведения Новогодней лотереи 25 декабря 2022 года с 13:00 до 15:00 МСК по адресу г. Москва, ул. Вешняковская, д.18 на 4 этаже ТЦ «Вешняки», при личном присутствии предъявителя купона.</p>
					<p>Предложение об участии в Новогодней лотерее, а также Правила участия, будут сопровождаться размещением соответствующей информации на печатных рекламных материалах (листовках и иных печатных материалах) в торговом центре «ВЕШНЯКИ»,  более подробную информацию о количестве и перечне призов, порядке их получения Вы можете узнать на сайте торгового центра «ВЕШНЯКИ»  <a href="https://www.tc-veshnyaki.ru">http://www.tc-veshnyaki.ru</a>, либо у администратора Торгового центра по телефону <a href="tel:84955104521">8 (495) 510-45-21</a>.</p>

					<p>С момента получения Приза его обладатель самостоятельно несет налоговую ответственность за уплату всех применимых налогов, сборов и иных обязательных платежей согласно законодательству РФ. </p>
					<p>Обязательства Организатора относительно качества Призов ограничены гарантиями, предоставленными их производителями. Целостность и функциональная пригодность Призов проверяются Победителями непосредственно при их получении.</p>
					<p>Право собственности, а также риск случайной гибели, или повреждение Приза с момента его фактической передачи Участнику Победителю переходит к Участнику лотереи. Для получения Приза выигравшему Участнику необходимо предъявить документ, удостоверяющий личность: паспорт гражданина Российской Федерации.</p>
					<p>Организатор Новогодней лотереи вправе обращаться к Участникам/ Победителям с просьбой принять участие в интервьюировании, фото- и/или видеосъёмке без выплаты за это дополнительного вознаграждения. В случае согласия Победителя в интервьюировании, фото- и/или видеосъёмке такой Участник обязуется безвозмездно предоставить Организатору права на использование его имени, фамилии, фотографий и иных материалов, изготовленных в связи с участием Победителя в Новогодней лотереи. Авторские (смежные) права на изготовленные с участием Победителя материалы принадлежат Организатору. Факт участия в Новогодней лотереи подразумевает, что её Участники ознакомлены и согласны с настоящими Правилами. </p>
					<p>Согласие с настоящими Правилами является полным и безоговорочным.</p>
					<p>Принимая участие в Новогодней лотереи, Участник осознает и соглашается, что информация о его участии (а в случае победы также его фотографии и ФИО) может быть размещена в сети Интернет и/или других рекламных материалах, связанных с продвижением торгового центра «ВЕШНЯКИ», дает свое согласие на обработку и передачу персональных данных, в соответствии с Федеральным законом от 27.07.2006 № 152-ФЗ «О персональных данных».</p>
									
									
				
				</div>
			</section>
			
			<!-- ********* -->
			<!-- 4TH BLOCK -->
			<section class="sect sect-callback gal" id="callback">
				<div class="content dis-fl fl-colnw jsc-flst alit-flst">
					<div class="title">
						<h3>хотите к нам?</h3>
					</div>
					<form action="" class="post-form" id="form-callback" name="form-callback" method="post" enctype="multipart/form-data" accept-charset="UTF-8" onsubmit="return valForm(this)">
						<div class="dis-fl fl-rwwr jsc-spbet alit-flst">
							<div class="input">
								<span>Имя</span>
								<input type="text" id="name" placeholder="" name="imya" required="">
							</div>
							<div class="input">
								<span>Телефон</span>
								<input type="tel" id="phone" placeholder="" name="phone" required="">
							</div>
							<div class="input">
								<span>Компания</span>
								<input type="email" id="email" placeholder="" name="email" required="">
							</div>
						</div>
						<button class="btn btn-form" type="submit">отправить</button>
					</form>
				</div>
			</section>
			<!-- ********* -->
			<!-- FOOTER -->
			<footer class="gal" id="contacts">
				<div class="map" id="map"></div>
				<div class="container dis-fl fl-rwwr jsc-spbet alit-ct">
					<div class="address">
						<p>Адрес <span>г. Москва,<br>Вешняковская улица, 18</span></p>
					</div>
					<div class="phone">
						<p>По вопросам аренды <a href="tel:<?=$config['phone_link']?>"><?=$config['phone_text']?></a></p>
						<img src="../img/footer-arrow.svg" alt="" id="fimg-1">
						<img src="../img/footer-arrow-2.svg" alt="" id="fimg-2">
					</div>
					<div class="whours">
						<p>Часы работы <span>С 10.00 до 23.00</span></p>
					</div>
				</div>
			</footer>
		</main>
		<!-- styles -->
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"> -->
		<link rel="stylesheet" href="../css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="../css/slick.css">
		<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css">
		<!-- scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!-- <script src="../js/jquery.scrolla.min.js"></script> -->
		<script src="../js/jquery.maskedinput.js"></script>
		<script src="../js/jquery.fancybox.min.js"></script>
		<script src="../js/slick.min.js"></script>
		<script src="../js/ofi.min.js"></script>
		<script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
		<script src="../js/map.js?v=<?php echo filectime('../js/map.js'); ?>"></script>
		<script src="../js/gallery.js?v=<?php echo filectime('../js/gallery.js'); ?>"></script>
		<script src="../js/form.js"></script>

    <script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>
    </body>
</html>